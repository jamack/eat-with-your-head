# Jedz-z-Głową #
(eat-with-your-head/healty-eating)

### Opis ###

Aplikacja przeznaczona do pilnowania sposobu odżywiania użytkownika. 
Pozwala na sprawdzanie składników odżywczych każdego posiłku i przechowywanie ich w bazie danych. 
Użytkownik może określić swój wiek, wagę i wzrost, aby obliczyć dzienne spożycie kalorii. 
Użytkownik może dodać swoje posiłki i sprawdzić, czy prawidłowo się odżywia. 


### Funkcjonalności ###

Zarządzanie posiłkami i zapisywanie ich w bazie danych. 
Określenie składników odżywczych (tłuszcze, białka i węglowodany na 100g). 
Śledzenie spożycia kalorii poprzez określenie posiłków, które użytkownik zjadł w ciągu dnia. 

### Repozytorium ###

https://bitbucket.org/jamack/eat-with-your-head/

